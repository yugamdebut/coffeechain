const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect("mongodb://localhost/coffeechains",{useUnifiedTopology: true, useNewUrlParser: true })
.then(() => {
    console.log("Connection successfully established");    
}).catch(err => {
    console.log('Could not connect to the database.');
    console.log(err);
    process.exit();
});