$(document).ready(function() {
    $('#firstName,#lastName').keypress(function CheckIsCharacter(e) {
        var charCode = (e.which) ? e.which : event.keyCode
        if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode===32)) 
        {
            return true;   
        }
        else 
        {               
            return false;        
        }     
    });
    $("#addClient").validate({
        rules: {
            firstName:{
                required: true,
                maxlength: 20,
            },
            lastName:{
                required: true,
                maxlength: 20,
            },
            username: {
                required: true,
                email: true
            },
            password:{
                required:true,
                minlength:8,
                maxlength:12
            }
        },
        messages: {
            firstName:{
                required:"Please enter first name",
                maxlength:"Your First name must consist of at most 20 characters"
            },
            lastName:{
                required:"Please enter last name",
                maxlength:"Your Last name must consist of at most 20 characters"
            },
            username: {
                required:"Please enter email",
                email:"Please enter correct email"
            },       
            password:{
                required:"Please enter password",
                minlength:"Password should be atleast 8 characters",
                maxlength:"Password shoult not exceed 12 characters"
            }
        }
    }); 
});