$(document).ready(function() {
    $("#admin_login").validate({
        rules: {
            username: {
                required: true,
                email: true
            },
            password:{
                required:true
            },
            phone:{
                required:true,
                maxlength: 10,
            }
        },
        messages: {
            username: {
                required:"Please enter email",
                email:"Please enter correct email"
            },       
            password:{
                required:"Please enter password"
            }
        }
    }); 
});