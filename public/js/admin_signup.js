$(document).ready(function() {
    $('#firstName,#lastName').keypress(function CheckIsCharacter(e) {
        var charCode = (e.which) ? e.which : event.keyCode
        if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode===32)) 
        {
            return true;   
        }
        else 
        {               
            return false;        
        }     
    });
    $('#phone').keypress(function CheckIsNumeric(e) {
        var charCode = (e.which) ? e.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)) 
            return false;
        else 
            return true;
    });
    $('#phone').keypress(function checklength(){
        if(this.value.length > 9) return false
    });
    $('INPUT[type="file"]').change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];
        switch (ext) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
                $('#submit').attr('disabled', false);
                break;
            default:
                alert('Please select jpg or png file.');
                this.value = '';
        }
    });
    $("#admin_signup").validate({
        rules: {
            firstName:{
                required: true,
                maxlength: 20,
            },
            lastName:{
                required: true,
                maxlength: 20,
            },
            username: {
                required: true,
                email: true
            },
            password:{
                required:true,
                minlength:8,
                maxlength:12
            },
            phone:{
                required:true,
                maxlength: 10,
            },
            file:   {
                required: true,
                extension: "jpg|png|jpeg"
            }
        },
        messages: {
            firstName:{
                required:"Please enter first name",
                maxlength:"Your First name must consist of at most 20 characters"
            },
            lastName:{
                required:"Please enter last name",
                maxlength:"Your Last name must consist of at most 20 characters"
            },
            username: {
                required:"Please enter email",
                email:"Please enter correct email"
            },       
            password:{
                required:"Please enter password",
                minlength:"Password should be atleast 8 characters",
                maxlength:"Password shoult not exceed 12 characters"
            },
            phone:{
                required:"Please enter phone number",
                maxlength:"Please enter correct phone number"
            },
            file:"Please select profile image"
        }
    }); 
});