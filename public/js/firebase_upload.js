$(document).ready(function() {
    $("#file").change(function(){
        // Created a Storage Reference with root dir
        var storageRef = firebase.storage().ref();
        // Get the file from DOM
        var file = $('#file').prop('files')[0];
        var thumb = document.getElementById("thumbnail");
        console.log(file);
        // Create the file metadata
        var metadata = {
        contentType: 'image/png'
        };
        /// Upload file and metadata to the object 'images/file.jpg'
        var uploadTask = storageRef.child('images/' + file.name).put(file, metadata);   
        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
        (snapshot) =>{  }, (error) =>{
            switch (error.code) {
            case 'storage/unauthorized':
                console.log("Unauth");
                break;
            case 'storage/canceled':
                console.log("cancelled");
                break;
    
            case 'storage/unknown':
                // Unknown error occurred, inspect error.serverResponse
                break;
            }
        }, () =>{
            // Upload completed successfully, now we can get the download URL
            uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) =>{
                console.log('File available at', downloadURL);
                $('#thumbnail').attr('src',downloadURL);
                $('#firebase_path').val(downloadURL);
            });
            });
    });
});