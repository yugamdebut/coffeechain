const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt');
var users = require('../model.js');


function initialize(passport, getUserByEmail, getUserById) {
  const authenticateUser = async (username, password, done) => {
    const users = getUserByEmail(username);
    if (users.username == null) {
      return done(null, false, { message: 'No users with that email' })
    }

    try {
      if (await bcrypt.compare(password, users.password)) {
        return done(null, users)
      } else {
        return done(null, false, { message: 'Password incorrect' })
      }
    } catch (e) {
      return done(e)
    }
  }

  passport.use(new LocalStrategy({ usernameField: 'username' }, authenticateUser))
  passport.serializeUser((users, done) => done(null, users.id))
  passport.deserializeUser((id, done) => {
    return done(null, getUserById(id))
  })
}

module.exports = initialize