var users = require('./model.js');
const bcrypt = require('bcrypt');
var passwordHash = require('password-hash');
const saltRounds = 10;

class User1 {
    async add(data) {
        try{
            let find_user = await users.findOne({ username: data.username });
            if (!find_user) {
                var hashedPassword = passwordHash.generate(data.password);
                data.password = hashedPassword;
                let data1 = await users.create(data);
                    console.log(data1);
                    if (data1) {
                        return Promise.resolve(data1);
                    }
            }
            else {
                return Promise.reject();
            }
        }
        catch(err){
            console.log(err);
        }
    }
}
module.exports = User1;