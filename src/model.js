const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');


const userSchema = new Schema({
    firstName: { type: String,required: true },
    lastName: { type: String},
    username:{type:String},
    password: { type: String},
    phone: {  type: String },
    firebase_path: { type: String},
    type:{ type: Number,default:1},
    status: {type :Number, default: 0},
    is_deleted:{type:Number,default:0}},
    {
        timestamps:{createdAt:'created_at',updatedAt:'updated_at'}
    }
);
userSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("users", userSchema);