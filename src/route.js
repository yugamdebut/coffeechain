var express = require('express');
var router = express.Router();
var User1 = require('./controller');
var users = require('./model.js');
var objUser = new User1();
var passport = require('passport');
var passwordHash = require('password-hash');
var LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');


/* GET home page. */
router.get('/', async (req, res, next)=> {
  res.render('signIn',{message: req.flash()});
});

/* GET home page. */
router.get('/signup', async (req, res, next)=> {
  res.render('signUp');
});

// Create a new User
router.post('/signup',async (req, res, next) => {
  objUser.add(req.body).then(result => {
    req.flash('success', 'User added!')
    console.log('user added successfully');
    res.locals.message = req.flash();
    res.redirect('/');
  }).catch(err => {
    console.log(err);
    req.flash('danger', 'user not added!')
    res.locals.error = req.flash();
    res.redirect('/signUp');
  });
});

// //login user account
router.post('/login',passport.authenticate('local',
  {failureRedirect: '/',failureFlash:true,}), (req, res, user_data) => {
    try {
      if (!user_data) { //if user not found (incorrect usernmae or password)
      req.flash('error', 'Invalid credentials');
      res.redirect('/');
    }
    //check for users
    if (user_data.is_deleted == 1) {
      req.flash('error', 'your account deleted by admin');
      res.redirect('/')
    }
    req.flash('success', 'welcome to the dashboard');
    res.redirect('/dashboard');

    } catch (err) {
    res.status(400).send({ message: err.message, status: 0 });
    }
    console.log(req.user)
});

/* GET dashboard page. */
router.get('/dashboard', async (req, res, next)=> {
  if (req.isAuthenticated()) {
    res.render('dashboard',{user : req.user});
  }else{
    res.redirect('/');
  }
});

router.get('/addClient', async (req, res, next)=> {
    res.render('addClient',{user : req.user,active_menu: "clients",});
});

router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

passport.use(new LocalStrategy({ usernameField: 'username', passwordField: 'password', passReqToCallback: true },
  async (req, username, password, done) => {
  try {
    let data = await users.findOne({ username: username});
  if (!data) {
    return done(null, false);
  }
  if (!passwordHash.verify(password, data.password)) {
    req.flash('error', 'please enter the right password ');
    return done(null, false);
  }

  return done(null, data);
  } catch (err) {
    console.log("error in passport");
    console.log(err);
  }
  }));
  passport.serializeUser((users, done) => {
    done(null, users._id);
  });

  passport.deserializeUser(async (id, done) => {
    let user_data = await users.findOne({ _id: id });
    user_data = JSON.parse(JSON.stringify(user_data));
    done(null, user_data);
});

module.exports = router;
